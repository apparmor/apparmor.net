---
title: AppArmor 3.0.9 released
date: 2023-02-27
---

AppArmor 3.0.9 is a maintenance release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all currently supported upstream kernels.

The kernel portion of the project is maintained and pushed separately.


# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.0.9

### Launchpad
  -   <https://launchpad.net/apparmor/3.0/3.0.9/+download/apparmor-3.0.9.tar.gz>
  -   sha256sum: fd96dc4a4145fce2b7282a3c19ffab70a4003c0953ed5992cfd7820df7215f02
  -   signature: <https://launchpad.net/apparmor/3.0/3.0.9/+download/apparmor-3.0.9.tar.gz.asc>
  -   sha256sum: bcdd447c12171dc419ff8cb99928a4b166a6805c5d47cb09e759b53f1c3cf16a

# Changes in this Release

These release notes cover all changes between 3.0.8 (474a12ebe86bb9314e482f918c589b484fd9ec2a) and 3.0.9 (af9d04d24b8d4735b76a0603db6f6017da02d403) on [apparmor-3.0 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.0).


## libapparmor
- add support for "class" field in logparsing
- add support for "requested" and "denied" fields in logparsing
- add scanner support for dbus "method" field ([MR:958](https://gitlab.com/apparmor/apparmor/-/merge_requests/958), [HUBMR:286](https://github.com/bus1/dbus-broker/pull/286))

## Policy Compiler (a.k.a apparmor_parser)
- Fix mode not being printed when debugging AF_UNIX socket rules. ([MR:979](https://gitlab.com/apparmor/apparmor/-/merge_requests/979))
- Fix spacing when printing out AF_UNIX addresses ([MR:978](https://gitlab.com/apparmor/apparmor/-/merge_requests/978))
- Fix invalid reference to transitions when building the chfa ([MR:956](https://gitlab.com/apparmor/apparmor/-/merge_requests/956), [AABUG:290](https://gitlab.com/apparmor/apparmor/-/issues/290))

## Bin Utils
- aa-status
  - Fix malformed json output with unconfined processes ([MR:964](https://gitlab.com/apparmor/apparmor/-/merge_requests/964), [AABUG:295](https://gitlab.com/apparmor/apparmor/-/issues/295))


## Utils
- Fix log parsing crash due to bad event ([MR:959](https://gitlab.com/apparmor/apparmor/-/merge_requests/959))


## Policy

#### abstractions
- Add abstractions/groff with lots of groff/nroff helpers ([MR:973](https://gitlab.com/apparmor/apparmor/-/merge_requests/973), [BOO:1065388](https://bugzilla.opensuse.org/show_bug.cgi?id=1065388))
- audio
  - Add access to pipewire client.conf ([MR:970](https://gitlab.com/apparmor/apparmor/-/merge_requests/970), [LP:2003702](https://bugs.launchpad.net/bugs/2003702))
- crypto
  - allow access to hwf.deny ([MR:961](https://gitlab.com/apparmor/apparmor/-/merge_requests/961))
- openssl
    - allow reading /etc/ssl/openssl-*.cnf ([MR:984](https://gitlab.com/apparmor/apparmor/-/merge_requests/984), [BOO:1207911](https://bugzilla.opensuse.org/show_bug.cgi?id=1207911))
- nvidia
  - add new cache directory ([MR:982](https://gitlab.com/apparmor/apparmor/-/merge_requests/982))
  - allow reading @{pid}/comm ([MR:954](https://gitlab.com/apparmor/apparmor/-/merge_requests/954))
- nvidia_modprobe
  - update for driver families and /sys path ([MR:983](https://gitlab.com/apparmor/apparmor/-/merge_requests/983))
- samba
  - allow modifying /var/cache/samba/*.tdb ([MR:988](https://gitlab.com/apparmor/apparmor/-/merge_requests/988))
- ssl_certs
  - allow access to all entries in pki/trust/ ([MR:961](https://gitlab.com/apparmor/apparmor/-/merge_requests/961))
- ubuntu-helpers
  - Fix: Opening links with Brave ([MR:957](https://gitlab.com/apparmor/apparmor/-/merge_requests/957), [AABUG:292](https://gitlab.com/apparmor/apparmor/-/issues/292))

#### profiles
- avahi-daemon
  - needs attach_disconnected ([MR:960](https://gitlab.com/apparmor/apparmor/-/merge_requests/960))
- dnsmasq
  - add Waydroid pid file ([MR:969](https://gitlab.com/apparmor/apparmor/-/merge_requests/969))
- lsb_release
  - allow cat and cut ([MR:953](https://gitlab.com/apparmor/apparmor/-/merge_requests/953))
-  nscd
   - allow using systemd-userdb ([MR:977](https://gitlab.com/apparmor/apparmor/-/merge_requests/977))
- postfix-tlsmgr
  - allow reading openssl.cnf ([MR:981](https://gitlab.com/apparmor/apparmor/-/merge_requests/981))
- samba*
  - allow access to pid files directly in /run/ ([MR:988](https://gitlab.com/apparmor/apparmor/-/merge_requests/988))
- smbd
  - allow reading /var/lib/nscd/netgroup ([MR:948](https://gitlab.com/apparmor/apparmor/-/merge_requests/948))


## Tests
- fix bogon patch characters in Makefile ([MR:963](https://gitlab.com/apparmor/apparmor/-/merge_requests/963))

