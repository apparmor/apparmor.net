---
title: AppArmor 3.0.12 released
date: 2023-06-21
---

AppArmor 3.0.12 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

This release fixes a regression in 3.0.11 introduced by its fix to mount rule generations.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.0.12

### Launchpad
  -   <https://launchpad.net/apparmor/3.0/3.0.12/+download/apparmor-3.0.12.tar.gz>
  -   sha256sum: 7c7aed0b68d9e73fa1bb6665ece65a704217fb32dbad04ae01cb5f6d8d24ab31
  -   signature: <https://launchpad.net/apparmor/3.0/3.0.12/+download/apparmor-3.0.12.tar.gz.asc>
  -   signature sha256sum: 1b5152634072c03b690ef35c5243489679e4ca4903ee904209b687950f61991b

# Changes in this Release

These release notes cover all changes between 3.0.11 (tag v3.0.11) ) and 3.0.12 (tag v3.0.12) on the [apparmor-3.0 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.0).


## Policy Compiler (a.k.a apparmor_parser)
- fix rule flag generation change_mount type rule ([MR:1054](https://gitlab.com/apparmor/apparmor/-/merge_requests/1054), [BOO:1211989](https://bugzilla.opensuse.org/show_bug.cgi?id=1211989), Fixes: [LP:2023814](https://bugs.launchpad.net/bugs/2023814))


## Policy

#### abstractions
- base
  - Add transparent hugepage support ([MR:1050](https://gitlab.com/apparmor/apparmor/-/merge_requests/1050))
- authentication
  -  Add GSSAPI mechanism modules config ([MR:1049](https://gitlab.com/apparmor/apparmor/-/merge_requests/1049))
