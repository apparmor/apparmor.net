---
title: AppArmor 3.1.5 released
date: 2023-06-09
---

AppArmor 3.1.5 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

This release fixes a regression in 3.1.4 introduced by the fix to mount rule generations.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.1.5

### Launchpad
  -   <https://launchpad.net/apparmor/3.1/3.1.5/+download/apparmor-3.1.5.tar.gz>
  -   sha256sum: a7cf4b792dd88eb1ac18104b246529662a8a66b733c3392daa2b384bbfa064f8
  -   signature: <https://launchpad.net/apparmor/3.1/3.1.5/+download/apparmor-3.1.5.tar.gz.asc>
  -   signature sha256sum: e09e813bf7314c034bc5979167332dcee8b7de57bea17b09548413a2827fe8b9

# Changes in this Release

These release notes cover all changes between 3.1.4 (e5a3c03357df1c279a529f8bd8ac0b82e26b427f) and 3.1.5 (3c5d40e84a7bed499e5de36dbe91557e2d302606) on the [apparmor-3.1 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.1).


## Policy Compiler (a.k.a apparmor_parser)
- fix parsing of source as mount point for propagation type flags ([MR:1048](https://gitlab.com/apparmor/apparmor/-/merge_requests/1048), [LP:1648245](https://bugs.launchpad.net/bugs/1648245), [LP:1648245](https://bugs.launchpad.net/bugs/1648245))
- Fix use-after-free of 'name' ([MR:1040](https://gitlab.com/apparmor/apparmor/-/merge_requests/1040))
- Fix order of if conditions to avoid unreachable code ([MR:1039](https://gitlab.com/apparmor/apparmor/-/merge_requests/1039))

## Utils
- aa-status
  - Fix invalid json output ([MR:1046](https://gitlab.com/apparmor/apparmor/-/merge_requests/1046))

## Policy

#### abstractions
- base
  -  allow reading of /etc/ld-musl-*.path ([MR:1047](https://gitlab.com/apparmor/apparmor/-/merge_requests/1047),
    [AABUG:333](https://gitlab.com/apparmor/apparmor/-/issues/333))
- snap_browsers
  - add lock file permission to snap browsers ([MR:1045](https://gitlab.com/apparmor/apparmor/-/merge_requests/1045))
