---
title: AppArmor 4.0.2 released
date: 2024-07-23
---

AppArmor 4.0.2 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

AppArmor 4.0.2 does not address interactions between the bwrap_userns_restrict and flatpak profiles. The bwrap profile is not enabled by default, if enabled the flatpak profile needs to be updated.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v4.0.2

# Changes in this Release

These release notes cover all changes between 4.0.1 (b0eb95457bc2de401920308869d016e696c73664) ) and 4.0.2 (84a6bc1b6dcdfeabb1ed3597f01e314f3bcee5c1) on the [apparmor-4.0 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-4.0).


## Init
- aa-teardown
  - print out which profile removal failed ([MR:1257](https://gitlab.com/apparmor/apparmor/-/merge_requests/1257))
  - fix removal for `unconfined` profiles ([MR:1242](https://gitlab.com/apparmor/apparmor/-/merge_requests/1242), [BOO:1225457](https://bugzilla.opensuse.org/show_bug.cgi?id=1225457))


## Libraries
- Partial revert of "libapparmor: add log parser support for saddr, daddr, src and dest ([MR:1250](https://gitlab.com/apparmor/apparmor/-/merge_requests/1250))
- Honor global CFLAGS when building Python library ([MR:1254](https://gitlab.com/apparmor/apparmor/-/merge_requests/1254))
- add log parser support for saddr, daddr, src and dest ([MR:1239](https://gitlab.com/apparmor/apparmor/-/merge_requests/1239), [AABUG:397](https://gitlab.com/apparmor/apparmor/-/issues/397))


## Policy Compiler (a.k.a apparmor_parser)
- make lead # in assignment value indicate a comment ([MR:1255](https://gitlab.com/apparmor/apparmor/-/merge_requests/1255), [AABUG:407](https://gitlab.com/apparmor/apparmor/-/issues/407))
- ensure mqueue obeys abi ([MR:1277](https://gitlab.com/apparmor/apparmor/-/merge_requests/1277), [AABUG:412](https://gitlab.com/apparmor/apparmor/-/issues/412))
- don't add mediation classes to unconfined profiles ([MR:1247](https://gitlab.com/apparmor/apparmor/-/merge_requests/1247), [LP:2067900](https://bugs.launchpad.net/bugs/2067900))
- fix all rule to properly add unix rule mediation ([MR:1273](https://gitlab.com/apparmor/apparmor/-/merge_requests/1273), [AABUG:410](https://gitlab.com/apparmor/apparmor/-/issues/410))
- fix Normalization infinite loop ([MR:1252](https://gitlab.com/apparmor/apparmor/-/merge_requests/1252), [AABUG:398](https://gitlab.com/apparmor/apparmor/-/issues/398))


## Utils
- Include abi/4.0 when creating a new profile ([MR:1231](https://gitlab.com/apparmor/apparmor/-/merge_requests/1231), [AABUG:392](https://gitlab.com/apparmor/apparmor/-/issues/392))
- MountRule: add support for quoted paths and empty source ([MR:1259](https://gitlab.com/apparmor/apparmor/-/merge_requests/1259), [BOO:1226031](https://bugzilla.opensuse.org/show_bug.cgi?id=1226031))
- MountRule: Aligning behavior with apparmor_parser ([MR:1237](https://gitlab.com/apparmor/apparmor/-/merge_requests/1237), [LP:2065685](https://bugs.launchpad.net/bugs/2065685))
- MountRule: Relaxing constraints on fstype and completing AARE support ([MR:1198](https://gitlab.com/apparmor/apparmor/-/merge_requests/1198), [MR:1228](https://gitlab.com/apparmor/apparmor/-/merge_requests/1228))
- Ignore/skip disabled profiles ([MR:1264](https://gitlab.com/apparmor/apparmor/-/merge_requests/1264))
- ignore events for missing profiles ([MR:1265](https://gitlab.com/apparmor/apparmor/-/merge_requests/1265))
- test-logprof: increase timeout ([MR:1268](https://gitlab.com/apparmor/apparmor/-/merge_requests/1268))
- Handle mount events/log entries without class ([MR:1229](https://gitlab.com/apparmor/apparmor/-/merge_requests/1229), [LP:1648143](https://bugs.launchpad.net/bugs/1648143), [LP:1689667](https://bugs.launchpad.net/bugs/1689667))
- Don't rely on argparse saying "options:" ([MR:1226](https://gitlab.com/apparmor/apparmor/-/merge_requests/1226))
- Fix redefinition of _ ([MR:1218](https://gitlab.com/apparmor/apparmor/-/merge_requests/1218), [AABUG:387](https://gitlab.com/apparmor/apparmor/-/issues/387))
- aa-notify: fix translation of an error message ([MR:1271](https://gitlab.com/apparmor/apparmor/-/merge_requests/1271))
- aa-remove-unknown: fix removing unknown profiles that contain spaces ([MR:1240](https://gitlab.com/apparmor/apparmor/-/merge_requests/1240), [MR:1243](https://gitlab.com/apparmor/apparmor/-/merge_requests/1243), [AABUG:395](https://gitlab.com/apparmor/apparmor/-/issues/395))
- allow `/` as mqueue name ([MR:1279](https://gitlab.com/apparmor/apparmor/-/merge_requests/1279), [AABUG:413](https://gitlab.com/apparmor/apparmor/-/issues/413))

## apparmor.vim
- add support for userns and the unconfined flag ([AABUG:396](https://gitlab.com/apparmor/apparmor/-/issues/396))


## Policy

#### abstractions
- fcitx5
  - add dbus interface ([MR:1222](https://gitlab.com/apparmor/apparmor/-/merge_requests/1222), [HUBMR:12924](https://github.com/snapcore/snapd/pull/12924))
- nameservice
  - allow reading @{PROC}/@{pid}/net/ipv6_route ([MR:1246](https://gitlab.com/apparmor/apparmor/-/merge_requests/1246))
- wutmp
  - allow writing wtmpdb ([MR:1267](https://gitlab.com/apparmor/apparmor/-/merge_requests/1267))
- X
  - add another xauth path ([MR:1249](https://gitlab.com/apparmor/apparmor/-/merge_requests/1249), [BOO:1223900](https://bugzilla.opensuse.org/show_bug.cgi?id=1223900))


#### profiles
- bwrap-userns-restrict
  - add mediate_deleted to bwrap ([MR:1272](https://gitlab.com/apparmor/apparmor/-/merge_requests/1272))
- chromium
  - Add userns stub chromium variants ([MR:1238](https://gitlab.com/apparmor/apparmor/-/merge_requests/1238), [AABUG:394](https://gitlab.com/apparmor/apparmor/-/issues/394))
- firefox
  - allow /etc/writable/timezone and update UPower DBus access ([MR:1253](https://gitlab.com/apparmor/apparmor/-/merge_requests/1253), [AABUG:409](https://gitlab.com/apparmor/apparmor/-/issues/409))
- php-fpm
  - installation of php-fpm needs w @{run}/systemd/notify ([MR:1251](https://gitlab.com/apparmor/apparmor/-/merge_requests/1251), [LP:2061113](https://bugs.launchpad.net/bugs/2061113))
- plasmashell
  - Add openSUSE path to profile ([MR:1248](https://gitlab.com/apparmor/apparmor/-/merge_requests/1248), [BOO:1225961](https://bugzilla.opensuse.org/show_bug.cgi?id=1225961))
- samba
  - add fixes for samba from issue #386 ([MR:1219](https://gitlab.com/apparmor/apparmor/-/merge_requests/1219), [AABUG:386](https://gitlab.com/apparmor/apparmor/-/issues/386))
- samba-dcerpcd
  - allow to execute rpcd_witness ([MR:1256](https://gitlab.com/apparmor/apparmor/-/merge_requests/1256), [BOO:1225811](https://bugzilla.opensuse.org/show_bug.cgi?id=1225811))
- wike
  - fix location for wike profile and add unconfined profile for balena-etcher ([MR:1221](https://gitlab.com/apparmor/apparmor/-/merge_requests/1221))


## Infrastructure
- gitlab-ci.yml
  - fix pipeline for ubuntu:latest (noble) ([MR:1223](https://gitlab.com/apparmor/apparmor/-/merge_requests/1223), [AABUG:388](https://gitlab.com/apparmor/apparmor/-/issues/388))
