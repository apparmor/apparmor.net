---
title: AppArmor 3.0.13 released
date: 2024-02-02
---

AppArmor 3.0.13 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.0.13


# Changes in this Release

These release notes cover all changes between 3.0.12 (tag v3.0.12) ) and 3.0.13 (tag v3.0.13) on the [apparmor-3.0 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.0).


## Policy Compiler (a.k.a apparmor_parser)
- Fix subprofile name in profile serialization ([MR:1127](https://gitlab.com/apparmor/apparmor/-/merge_requests/1127))
- Fix typo in apparmor_parser manpage ([MR:1128](https://gitlab.com/apparmor/apparmor/-/merge_requests/1128))


## Policy

#### abstractions
- kde
  - Allow reading global Debian KDE settings ([MR:1056](https://gitlab.com/apparmor/apparmor/-/merge_requests/1056))
  - Fix plasma-browser-integration ([MR:1115](https://gitlab.com/apparmor/apparmor/-/merge_requests/1115))
- fonts 
  - Allow locking fontconfig user cache files ([MR:1057](https://gitlab.com/apparmor/apparmor/-/merge_requests/1057))
  - Allow writing to fontconfig user cache files ([MR:1059](https://gitlab.com/apparmor/apparmor/-/merge_requests/1059))
- pipewire
  - Allow use of client-rt.conf file ([MR:1113](https://gitlab.com/apparmor/apparmor/-/merge_requests/1113))
- nameservice
  - Allow reading /etc/authselect/nsswitch.conf ([MR:1119](https://gitlab.com/apparmor/apparmor/-/merge_requests/1119))
- wutmp 
  - Allow reading /run/systemd/sessions/ ([MR:1121](https://gitlab.com/apparmor/apparmor/-/merge_requests/1121))
#### profiles
- dovecot
  - Allow for the default dovecot libexecdir ([MR:1080](https://gitlab.com/apparmor/apparmor/-/merge_requests/1080))

## Tests

- Fail iopl/ioperm with lockdown on the syscall and capabilities tests ([MR:1063](https://gitlab.com/apparmor/apparmor/-/merge_requests/1063)/[MR:1064](https://gitlab.com/apparmor/apparmor/-/merge_requests/1064))
- Fix regression tests to run on kernels that only have network_v8 ([MR:1120](https://gitlab.com/apparmor/apparmor/-/merge_requests/1120))

## Tools
 
- Ignore ´//null-` peers in signal and ptrace events ([MR:1107](https://gitlab.com/apparmor/apparmor/-/merge_requests/1107))
- Fix aa-cleanprof to work with named profiles ([MR:1108](https://gitlab.com/apparmor/apparmor/-/merge_requests/1108))
- No longer skip exec events in hats ([MR:1135](https://gitlab.com/apparmor/apparmor/-/merge_requests/1135))
- Prevent ANSI terminal injection in aa-unconfined ([MR:1142](https://gitlab.com/apparmor/apparmor/-/merge_requests/1142))

## Misc

- Handle Incus in rc.apparmor ([MR:1112](https://gitlab.com/apparmor/apparmor/-/merge_requests/1112))
- Add ENOPROTOOPT error in aa_getcon() manpage ([MR:1143](https://gitlab.com/apparmor/apparmor/-/merge_requests/1143))
- Fix wrong syntax for profile stacking ([MR:1141](https://gitlab.com/apparmor/apparmor/-/merge_requests/1141))
