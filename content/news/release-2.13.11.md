---
title: AppArmor 2.13.11 released
date: 2024-02-02
---

AppArmor 2.13.11 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v2.13.11

# Changes in this Release

These release notes cover all changes between 2.13.10 (tag v2.13.10) ) and 2.13.11 (tag v2.13.11) on the [apparmor-2.13 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-2.13).

## Policy Compiler (a.k.a apparmor_parser)
- Fix subprofile name in profile serialization ([MR:1127](https://gitlab.com/apparmor/apparmor/-/merge_requests/1127))
- Deprecation warning should not have been backported ([MR:1129](https://gitlab.com/apparmor/apparmor/-/merge_requests/1129))


## Policy

#### abstractions
- kde
  - Allow reading global Debian KDE settings ([MR:1056](https://gitlab.com/apparmor/apparmor/-/merge_requests/1056))
  - Fix plasma-browser-integration ([MR:1115](https://gitlab.com/apparmor/apparmor/-/merge_requests/1115))
- fonts 
  - Allow locking fontconfig user cache files ([MR:1057](https://gitlab.com/apparmor/apparmor/-/merge_requests/1057))
  - Allow writing to fontconfig user cache files ([MR:1059](https://gitlab.com/apparmor/apparmor/-/merge_requests/1059))
- nameservice
  - Allow reading /etc/authselect/nsswitch.conf ([MR:1119](https://gitlab.com/apparmor/apparmor/-/merge_requests/1119))
#### profiles
- profiles: remove @{etc_ro} variable which is not available on 2.13 ([MR:1122](https://gitlab.com/apparmor/apparmor/-/merge_requests/1122))

## Tests

- Fail iopl/ioperm with lockdown on the syscall and capabilities tests ([MR:1063](https://gitlab.com/apparmor/apparmor/-/merge_requests/1063)/[MR:1064](https://gitlab.com/apparmor/apparmor/-/merge_requests/1064))
- Fix regression tests to run on kernels that only have network_v8 ([MR:1120](https://gitlab.com/apparmor/apparmor/-/merge_requests/1120))

## Tools
 
- Ignore ´//null-` peers in signal and ptrace events ([MR:1107](https://gitlab.com/apparmor/apparmor/-/merge_requests/1107))
- Prevent ANSI terminal injection in aa-unconfined ([MR:1142](https://gitlab.com/apparmor/apparmor/-/merge_requests/1142))

## Misc

- Add ENOPROTOOPT error in aa_getcon() manpage ([MR:1143](https://gitlab.com/apparmor/apparmor/-/merge_requests/1143))
- Fix wrong syntax for profile stacking ([MR:1141](https://gitlab.com/apparmor/apparmor/-/merge_requests/1141))
